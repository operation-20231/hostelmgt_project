import React, { useContext, useState } from 'react'
import { FaCheck } from 'react-icons/fa'
import DataContext from '../../utilities/DataContext'
import AuthContext from '../../utilities/AuthContext'
import { jwtDecode } from 'jwt-decode'
const D_Requests = () => {
  let { Hostels, displayLoader, getAllHostels } = useContext(DataContext)
  const { SERVER_URL } = useContext(AuthContext)
  const [loading, setLoading] = useState(false)
  async function approve(id) {
    setLoading(true)
    const response = await fetch(`${SERVER_URL}Hostel/${id}`, {
      method: 'PATCH',
      headers: {
        'Content-type': 'application/json',
        'Authorization': `Bearer ${JSON.parse(localStorage.getItem('authtokens')) && JSON.parse(localStorage.getItem('authtokens')).access}`
      },
      body: JSON.stringify({ status: "active" })
    })
    if (response.status == 202) {
      getAllHostels(setLoading)

      alert('Hostel Approved')
    }
    else {
      alert('Failed to update')
    }
    setLoading(false)
  }
  return (
    <>
      <div class="modal fade" id="RequestsModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen text-light">
          {
            loading ? displayLoader() : <div class="modal-content container bg-light p-0 my-3 rounded h-75">
              <div class="modal-header border-0 bg-Db ">

                <p class="modal-title fs-5" id="exampleModalLabel">Pending requests</p>
                <button type="button" class="btn-close bg-lighter" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body  row justify-content-center gap-2">
                <div class="list-group">
                  {
                    (Hostels.filter(hostel => hostel.status == "pending")).length > 0 ? <span class="list-group-item bg-db border-0 text-red br-0 d-flex align-items-center justify-content-between" aria-current="true">
                      <div className='col-lg-2 col-8'><span className="">Hostel</span> Name</div>
                      <div className='col-0 d-none d-lg-block'>Address</div>
                      <div className='col-2 d-none d-lg-block'>Contact</div>
                      <div className='col-2'></div>
                    </span> : <p className="text-light-blue text-center">No requests available</p>
                  }
                  {
                    Hostels ?
                      Hostels.map(hostel => (
                        hostel.status === "pending" && <span class="list-group-item bg-light hair-line text-Db d-flex align-items-center justify-content-between">  
                          <div className='col-lg-2 col-8'>{hostel.name}</div>
                          <div className='col-2 opacity-75 d-none d-lg-block'>{hostel.address}</div>
                          <div className='col-2 opacity-75 d-none d-lg-block'>{hostel.contact}</div>
                          <div className="col-3 mx-2"><button data-bs-dismiss="modal" type='button' onClick={() => approve(hostel.id)} className=' border-0 bg-red d-flex align-items-center justify-content-center rounded text-light approve py-2'><FaCheck className='mx-1' /> <small className=''>Approve</small></button></div>
                        </span>
                      ))
                      : <p>No Requests</p>
                  }

                </div>
              </div>
            </div>
          }
        </div>
      </div>
    </>
  )
}

export default D_Requests
