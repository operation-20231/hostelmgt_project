import React, { useEffect, useContext, useState} from "react";
import {
  FaHandPointRight,
} from "react-icons/fa";
import Stars from "./Stars";
import { Link } from "react-router-dom";
import AuthContext from "../utilities/AuthContext";
const Hostel = ({ hostel }) => {
  let [comment1, setComment1] = useState([])
  let { SERVER_URL } = useContext(AuthContext)

  useEffect(()=>{
    fetch(`${SERVER_URL}/feedback/${hostel.id}`)
      .then((res) => res.json())
      .then((data) => setComment1(data));

  },[])

  function reviewCount() {
    return comment1.length
  }

  return (
    <div className="card card-hostel hostel col-lg-3 bg-db text-light px-0 mx-lg-3 mx-0 br-none my-4 border-0 custom-shadow">
      <video controls  muted  >
        <source src={SERVER_URL+hostel.video} className="bg-red h-100"/>
      </video>
      <div className="card-body">
        <p className="card-title text-Db" >{hostel.name}</p>
        <p className="card-text d-flex align-items-center">
          <svg
            style={{ marginRight: 5, height: 15 }}
            xmlns="http://www.w3.org/2000/svg"
            width="16.277"
            height="23.452"
            viewBox="0 0 16.277 23.452"
          >
            <g id="location-sign-svgrepo-com" transform="translate(-60.531)">
              <path
                id="Path_6"
                data-name="Path 6"
                d="M68.669,0a8.148,8.148,0,0,0-8.138,8.139c0,4.32,7.384,14.412,7.7,14.84l.293.4a.182.182,0,0,0,.294,0l.293-.4c.315-.427,7.7-10.52,7.7-14.84A8.149,8.149,0,0,0,68.669,0Zm0,5.224a2.915,2.915,0,1,1-2.915,2.915A2.918,2.918,0,0,1,68.669,5.224Z"
                transform="translate(0)"
                fill="#ec1f46"
              />
            </g>
          </svg>
          <small className=" text-Db">{hostel.address}</small>
        </p>

        <Stars reviews={reviewCount()} rating={hostel.rating} />
        <Link className="nav-link" to={`/hostel/${hostel.id}`}>
          <small className="text-Db d-flex align-items-center justify-content-end">
            <FaHandPointRight style={{ marginRight: 10 }} />
            View More
          </small>
        </Link>
      </div>
    </div>
  );
};

export default Hostel;
