import {useContext, useState, useEffect, useLayoutEffect} from "react";
import AuthContext from "../utilities/AuthContext";
import moment from 'moment'
import DataContext from "../utilities/DataContext";
import { jwtDecode } from "jwt-decode";
import Stars from "./Stars";
import { FaUser, FaClock } from "react-icons/fa";
const Comment = ({ comment }) => {
  const timestamp = comment.created;
const datetime = moment(timestamp);
const relativeTime = moment(comment.created).fromNow()
  const {SERVER_URL} = useContext(AuthContext)
  const [loading, setLoading] = useState(true)
useEffect(()=>{
  setLoading(true)
    setLoading(false)
     
},[loading])
  return (
   <>
   {
    loading ? 
    
    <p>Loading....</p> : 
    


    <>
    <div className={`d-flex comment-card px-4 py-2 mx-3 rounded my-4 bg-blue text-light ${comment.email === jwtDecode((JSON.parse(localStorage.getItem('authtokens')).access)).email && ' bg-db'}`} style={{alignSelf:`${comment.email === jwtDecode((JSON.parse(localStorage.getItem('authtokens')).access)).email && 'self-end'}`}}>
     {
       comment.photo && <div className="small-rounded-image rounded-circle d-flex align-items-center justify-content-center" style={{ marginRight: 15 }}>
       <img src={SERVER_URL+comment.photo}/>
   </div>
     }
     <div className="comment-details text-light">

       <span>{
       comment.email
       ?
       comment.email == jwtDecode((JSON.parse(localStorage.getItem('authtokens')).access)).email 
       ?<span className="text-light">you</span>
       :comment.email.length > 12 ? comment.email.slice(0,12)+'...' : comment.email:
       comment.email.length > 12 ? comment.email.slice(0,12)+'...' : comment.email} </span><br />
       <small className="comment-date opacity-75"><FaClock/><span className="mx-2">{relativeTime}</span></small> <br />
       <Stars rating={comment.rating}/>
       <p className="my-2 text-light">{comment.body}</p>
       
     </div>
   </div>
   </>
   }
   </>
  );
};

export default Comment;
