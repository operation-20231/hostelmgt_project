import React, { useContext, useEffect, useState } from 'react'
import { FaStar, FaPaperPlane, FaSmile, FaAngry, FaLaugh, FaSadCry, FaSmileBeam, FaSadTear, FaLaughBeam, FaLaughSquint } from "react-icons/fa";
import { jwtDecode } from 'jwt-decode';
import DataContext from '../utilities/DataContext'
import AuthContext from '../utilities/AuthContext';
import { useParams } from 'react-router-dom';
const AddComment = () => {
  let [star1, setStar1] = useState(false)
  let [star5, setStar5] = useState(false)
  let [star2, setStar2] = useState(false)
  let [star3, setStar3] = useState(false)
  let [star4, setStar4] = useState(false)
  let [comment, setComment] = useState('')
  let { displayLoader, getComments,getAllHostels} = useContext(DataContext)
  let { SERVER_URL } = useContext(AuthContext)
  const [loading, setLoading] = useState(false)
  let [r, setR] = useState(0)
  const { id } = useParams()
  
  useEffect(()=>{
setR(1*(star1 ?1:0)+1*(star2 ?1:0)+1*(star3 ?1:0)+1*(star4 ?1:0)+1*(star5 ?1:0))
  },[star1,star2,star3,star4,star5])
  async function sendComment() {
    setLoading(true)
    if (comment.length < 1) {
      alert('Please type something in tthe comment section')
    } else {
      let rating = 0
      if (star1) {
        rating += 1
        
      }
      if (star2) {
        rating += 1
        
      }
      if (star3) {
        rating += 1
        
      }
      if (star4) {
        rating += 1
        
      }
      if (star5) {
        rating += 1
        
      }
      const response = await fetch(`${SERVER_URL}feedback/${id}`, {
        method: 'POST',
        headers: {
          'Content-type': 'application/json',
          'Authorization': `Bearer ${JSON.parse(localStorage.getItem('authtokens')).access}`
    
        },
        body: JSON.stringify({
          student: jwtDecode((JSON.parse(localStorage.getItem('authtokens')).access)).user_id,
          body: comment,
          rating: rating,
          hostel: id
        })
      })
      const data = response.json()
      if (response.status == 201) {
        getComments(id)
        getAllHostels()
        alert('Message sent successfully')
        setComment('')
        setStar1(false)
        setStar2(false)
        setStar3(false)
        setStar4(false)
        setStar5(false)
      } else {
        alert('Something went wrong')
      }
    }

    setLoading(false)
  }


  return (
    <>
      {
        loading ? displayLoader() : <div className="card px-0 br-none  bg-light text-light" >
          <div className="card-body px-4 py-2">
            <p className="review p-2 text-Db">leave reviews</p>
            <p className="card-text px-3">
              rating <br />{" "}
              <p className="lead d-flex align-items-center text-secondary justify-content-start">
                <FaStar onClick={() => setStar1(!star1)} className={`shine mx-1 ${star1 && 'text-warning'}`} /> <FaStar onClick={() => setStar2(!star2)} className={`shine mx-1 ${star2 && 'text-warning'}`} /> <FaStar onClick={() => setStar3(!star3)} className={`shine ${star3 && 'text-warning'}`} /> <FaStar onClick={() => setStar4(!star4)} className={`shine mx-1 ${star4 && 'text-warning'}`} /> <FaStar onClick={() => setStar5(!star5)} className={`shine mx-1 ${star5 && 'text-warning'}`} />
              <small className="text-light mx-4 bg-Db px-lg-4 px-1 py-2 rounded"><small className='d-flex align-items-center'><span className='d-none d-lg-block'>Click on the stars to rate</span>
              {
                r == 1?
                <FaAngry size={30} className='mx-1 text-warning'/>:
                r == 2?
                <FaSmile size={30} className='mx-1 text-warning'/>:
                r == 3?
                <FaLaugh size={30} className='mx-1 text-warning'/>:
                r == 4?
                <FaLaughBeam size={30} className='mx-1 text-warning'/>:
                r == 5?
                <FaLaughSquint size={30} className='mx-1 text-warning'/>:
                <FaSadTear size={30} className='mx-1 text-warning'/>
              }
                </small></small>
      
              
              </p>
            </p>
            <textarea
              onChange={(e) => setComment(e.target.value)}
              className="bg-lighter text-Db rounded mx-3 p-3"
              cols="40"
              rows="4"
              placeholder="Compose comment...….."
            ></textarea>
            <div
              style={{ width: 160 }}
              onClick={sendComment}
              className="my-3 px-2 bg-blue rounded-edges py-2 mx-3  d-flex align-items-center justify-content-center p-1"
            >
              <small>Send Comment</small>
              <FaPaperPlane className='mx-2' />
            </div>
          </div>
        </div>
      }
    </>
  )
}

export default AddComment
