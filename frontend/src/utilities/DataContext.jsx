import { createContext, useState, useEffect, useContext } from "react";
import { jwtDecode } from "jwt-decode";
import loaderIcon from '../assets/loader-icon.svg'
const DataContext = createContext({});
export const DataProvider = ({ children }) => {
  let SERVER_URL = "https://kigongovincent.pythonanywhere.com";
  const [loading, setLoading] = useState(true)
  const [currentRoom, setCurrentRoom] = useState(null)
  const [hostel, setHostel] = useState({});
  const [Hostels, setHostels] = useState([]);
  const [allUsers, setAllUsers] = useState([]);
  const [comments, setComments] = useState([]);
  let [myrooms, setMyRooms] = useState([])
  const [message, setmessage] = useState('');
  let [myhostel, setMyHostel] = useState({})
  const [rooms, setRooms] = useState([]);
  const [searchResults, setSearchResults] = useState([])
  const [reservationCount, setReservationCount] = useState(0)
  let [reservations, setReservations] = useState([])
  function displayLoader(){
    return (
      <div className="container my-5 py-5 d-flex align-items-center justify-content-center" style={{maxWidth:100}}><img src={loaderIcon} style={{maxWidth:100}} alt="" /></div>
    )
  }
  const getMyHostel=async(setLoading)=>{
    setLoading(true)
    const response = await fetch(`${SERVER_URL}/myHostel/${jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access).user_id}`)
    // const response = await fetch(`http://localhost:8000/myHostel/1`)
    if(response.status == 200){
      const data = await response.json()
            setMyHostel(data)
          const response2 = await fetch(`${SERVER_URL}/getRooms/${data.id}`)
          if(response2.status == 200){
        const data2 = await response2.json()
        setMyRooms(data2)
        setLoading(false)
      }
    }
    else if(response.status == 400){
setLoading(false)
    }
    setLoading(false)
    }
  const getAllUsers = async(setLoading) => {
    setLoading(true)
  const response = await fetch(`${SERVER_URL}/users/student`)
  const data =await response.json()
  if(response.status == 200 && data.length > 0){
    setAllUsers(data)
    setLoading(false)
  }
    setLoading(false)
  }
  const getAllHostels = async() => {
    setLoading(true)
   const response = await fetch(`${SERVER_URL}/getHostels`)
   const data = await response.json()
   if(response.status == 200){
    setHostels(data)
    setLoading(false)
   }
    setLoading(false)
  }

  useEffect(() => {
    setLoading(true)
    getAllHostels()
    window.scrollTo(0, 0);
    setLoading(false)
  }, [loading])

  function getReservations(){
    fetch(`${SERVER_URL}/myreservations/${jwtDecode((JSON.parse(localStorage.getItem('authtokens')).access)).user_id}`).then(res => res.json()).then(data => {
      setReservations(data) 
      setReservationCount(data.length >9? "9+": data.length)
  })
}


  const getHostel = (id, extra) => {
    setLoading(true)
    !extra ?
    fetch(`${SERVER_URL}/Hostel/${id}`,{
      headers:{
        'Authorization': `Bearer ${JSON.parse(localStorage.getItem('authtokens')) && JSON.parse(localStorage.getItem('authtokens')).access}`
  }})
      .then((res) => res.json())
      .then((data) => setHostel(data)) :
      fetch(`${SERVER_URL}/Hostel/${id}`,{
        method: 'PATCH',
        headers: {
          'Content-type': 'application/json',
          'Authorization': `Bearer ${JSON.parse(localStorage.getItem('authtokens')) && JSON.parse(localStorage.getItem('authtokens')).access}`
        },
        body: JSON.stringify(extra)
      })
      .then((res) => res.json())
      .then((data) => setHostel(data))
    setLoading(false)
  };



  function getRooms(id) {
    fetch(`${SERVER_URL}/getRooms/${id}`)
      .then((res) => res.json())
      .then((data) => setRooms(data));
  }


  const getComments = (id) => {
    fetch(`${SERVER_URL}/feedback/${id}`,{
      headers:{
        'Authorization': `Bearer ${JSON.parse(localStorage.getItem('authtokens')) && JSON.parse(localStorage.getItem('authtokens')).access}`
      }
    })
      .then((res) => res.json())
      .then((data) => setComments(data));
  }

  return (
    <DataContext.Provider
      value={{
        comments,
        Hostels,
        hostel,
        rooms,
        loading,
        currentRoom,
        reservationCount, 
        reservations,
        message, 
        searchResults, 
        myrooms, 
        myhostel, 
        allUsers,
        getAllUsers,
        setMyHostel,
        getMyHostel,
        setMyRooms,
        setSearchResults,
        setmessage,
        getReservations,
        setCurrentRoom,
        displayLoader,
        setLoading,
        setRooms,
        getHostel,
        getAllHostels,
        getRooms,
        getComments
      }}
    >
      {loading ?  displayLoader(): children}
    </DataContext.Provider>
  );
};
export default DataContext;
