import M_Navbar from "../components/manager/M_Navbar";
import M_Profile from "../components/manager/M_Profile";
import M_Stats from "../components/manager/M_Stats";
import React, { useContext, useEffect, useState } from "react"
import M_Reservations from "../components/manager/M_Reservations";
import AddRoom from '../components/AddRoom'
import { jwtDecode } from "jwt-decode";
import { Link } from "react-router-dom";
import DataContext from "../utilities/DataContext";
const ManagerHomePage = () => {
  const [loading, setLoading] = useState(true)
  const {getMyHostel}=useContext(DataContext)
  useEffect(() => {
    // getMyHostel(setLoading)
    setLoading(false)
  }, [])
  return (

    <>
      {
        JSON.parse(localStorage.getItem('authtokens')) ? jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access).groups[0] == "manager" ?
          !loading ? <div className="managerDashboard">
            <M_Navbar />
            <div className="container">
              <div className="mt-5 pt-5 d-lg-flex container justify-content-between alig-items-center">
                <M_Profile />
                <M_Stats />
              </div>
                <AddRoom />
              <M_Reservations />
            </div>
          </div>
            : '': <div className='text-center my-5 py-5 px-4 bg-db'><p className='text-light-blue'>You are not allowed here</p>
            <Link className='text-ok' to={'/'}>Back to home</Link>
            </div>:<div className='text-center my-5 py-5 px-4 bg-db'><p className='text-light-blue'>You are not allowed here</p>
            <Link className='text-ok' to={'/'}>Back to home</Link>
            </div>
      }

    </>
  );
};

export default ManagerHomePage;
