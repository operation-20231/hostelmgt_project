import React, { useContext } from "react";
import { jwtDecode } from "jwt-decode";
import MyProfile from "../components/MyProfile";
import Navbar from "../components/Navbar";
import Sidebar from "../components/Sidebar";

const Profile = () => {
  
  return (
  <>
  {
     JSON.parse(localStorage.getItem('authtokens')) ?  jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access).groups[0] == "student" ? 
    <>
<div className="fixed top">
<Navbar />
          <Sidebar />
</div>
      <div className="my-5 py-3">
          <div className="container d-flex justify-content-center ">
          <MyProfile user_id = {jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access).user_id}/>
          </div>
      </div>
    </>
    : <h2>You are not allowed here</h2>:<h1>Login required</h1>
       
      }
  </>
  );
};

export default Profile;
