import React, { useState, useEffect } from "react";
import Hostel from "./Hostel";
import DataContext from "../utilities/DataContext";
import { useContext } from "react";
const AllHostels = () => {
  const { Hostels } = useContext(DataContext);

  return (
    <div className="my-5 container">
       
          <small className="bg-red rounded-edges px-4 py-2 my-5">
            All Hostels available
          </small>
          <div className="row">
          {Hostels.length>0 ?Hostels.map((hostel) => (
            hostel.status == "active" && <Hostel hostel={hostel} key={hostel.id} />
          )): <p className="lead text-center text-light-blue my-4" >There are no hostels to show</p>}
          </div>
    </div>
  );
};

export default AllHostels;
