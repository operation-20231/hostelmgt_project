import React, { useContext, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import {
  FaHome,
  FaUser,
  FaCalendar,
  FaSignOutAlt,
  FaSearch,
} from "react-icons/fa";
import AuthContext from "../utilities/AuthContext";
import DataContext from "../utilities/DataContext";

const Sidebar = () => {
  let {Logout} = useContext(AuthContext)
  let {reservationCount, getReservations} = useContext(DataContext)
  let navigate = useNavigate()

  useEffect(()=>{
getReservations()
  },[])
  return (
    <div className="d-flex align-items-center justify-content-center flex-wrap pt-3 bg-Db">

      <Link className="nav-link d-flex align-items-center justify-content-start px-3 py-1 rounded" to="/student/dashboard">
        <FaHome className="m-2 " size={17} /><span className="d-none d-lg-block"> <small>Home</small></span>
      </Link>
      <div className="nav-link d-flex align-items-center justify-content-start m-1 px-3 py-1 rounded" data-bs-toggle="modal" data-bs-target="#AdvancedSearch" type="button">
        <FaSearch className="m-2 " size={17} /><span className="d-none d-lg-block" ><small>Advanced Search</small></span>

      </div>



      <Link className="nav-link d-none d-lg-flex d-flex align-items-center justify-content-start px-3 py-1 rounded" to="/reservation">
        <FaCalendar className="m-2 " size={17} /><span className="d-none d-lg-block"> <small>My Reservations</small></span>
        <p className="bg-red rounded-circle d-flex align-items-center justify-content-center" style={{ width: 30, height: 30 }}><small>{reservationCount}</small></p>
      </Link>
      <span className="mx-3">
      <Link className="nav-link d-lg-none reserve mx-lg-0 d-block d-flex align-items-center justify-content-start px-3  py-1 rounded" to="/reservation">
        <FaCalendar className="m-2 " size={17}/>
        <p className="bg-red rounded-circle d-flex align-items-center justify-content-center" style={{ width: 30, height: 30 }}><small>{reservationCount}</small></p>
      </Link>
      </span>


      <Link onClick={Logout} className="nav-link d-flex align-items-center justify-content-start  px-3 py-1 rounded" to={'/'}>
        <FaSignOutAlt className="m-2 " size={17} /><span className="d-none d-lg-block"> <small>Signout</small></span>
      </Link>

    </div>

  );
};

export default Sidebar;
