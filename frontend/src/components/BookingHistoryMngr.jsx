import React, { useContext } from "react";
//importing a custom context DataContext from the file path
import DataContext from "../utilities/DataContext";
import BookingMngr from "./BookingMngr";
//declaring a functional component using a JavaScript function
const BookingHistoryMngr = () => {
  //accessing reservations data using the useContext hook from the DataContext
  let { reservations } = useContext(DataContext);
  return (
    //using the JSX syntax extension to represent the structure of this component
    //it also describes how the UI should look like
    <div className="">
      <div className=" bg-lighter rounded py-3 bg-dark text-light px-4 row">
        <small className="col">Student</small>
        <small className="col">Room Number</small>
        <small className="col">Date</small>
        <small className="col">Amount</small>
      </div>
      <div className="bookinginfo">
        {reservations.map((reservation) => (
          <BookingMngr reservation={reservation} key={reservation.id} />
        ))}
      </div>
    </div>
  );
};

export default BookingHistoryMngr;
