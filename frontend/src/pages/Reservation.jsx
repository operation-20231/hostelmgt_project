import React from "react";

import AdvancedSearch from "../components/AdvancedSearch";
import MyReservation from "../components/MyReservation";
import Navbar from "../components/Navbar";
import Sidebar from "../components/Sidebar";

const Reservation = () => {
  return (
    <>
     <div className="fixed-top">
     <Navbar />
          <Sidebar />
     </div>
      <div className="mt-5 pt-5">
          <MyReservation />
      </div>
    </>
  );
};

export default Reservation;
