import React, { useContext } from 'react'
import DataContext from '../utilities/DataContext'
import { FaShieldAlt} from 'react-icons/fa'
const Notify = () => {
  let oldStyle = {
    position: 'fixed',
    bottom: '5%',
    right: '5%',
    transition: '.2s',
    zIndex:1000000000000
    
  }
  let newtyle = {
    position: 'fixed',
    bottom: '5%',
    right: '-100%',
    transition: '.2s'
    
  }
    let {message, setmessage} = useContext(DataContext)
  return (
    <div className={`message text-Db rounded overflow-hidden shadow-lg `} style={message?oldStyle:newtyle} >
      <p className="close bg-blue  d-flex px-2 py-2 justify-content-end align-items-center text-light" onClick={()=>setmessage('')}>Hide <FaShieldAlt className='mx-2 bg-0 text-light' size={17} /></p>
      <p className='px-4 py-3'>{message}</p>
    </div>
  )
}

export default Notify
