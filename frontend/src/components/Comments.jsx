import React, { useState, useEffect, useContext } from "react";
import Comment from "./Comment";
import DataContext from "../utilities/DataContext";
import { useParams } from "react-router-dom";
const Comments = ({h_id}) => {
  const { comments, getComments } = useContext(DataContext);
  let {id} = useParams()
  if(h_id){
    id = h_id
  }
  useEffect(()=>{
    getComments(id)
    
  },[])
  // Assuming you have a data source for comments
  return (
    <div className=" py-5 bg-ok" >
     <div className="container">
     <p className="text-center my-3 opacity=75">comments and reviews <span className="bg-red rounded px-2 py-1 mx-2">{comments.length}</span></p>
      <div className="d-flex flex-column" style={{maxHeight: 400, overflowY: 'scroll'}}>
        {comments.map((comment) => (
          <Comment comment={comment} key={comment.id} />
        ))}
      </div>
     </div>
    </div>
  );
};

export default Comments;
