import React, { useContext, useState } from 'react'
import Input from '../Input'
import DataContext from '../../utilities/DataContext'
import AuthContext from '../../utilities/AuthContext'
import { jwtDecode } from 'jwt-decode'
const M_Hostel = () => {
  const [name, setname] = useState('')
  const [email, setemail] = useState('')
  const [contact, setcontact] = useState('')
  const [address, setaddress] = useState('')
  const [image, setimage] = useState([])
  const [video, setvideo] = useState([])
  const {displayLoader,  getMyHostel} = useContext(DataContext)
  const {SERVER_URL} = useContext(AuthContext)
  const [loading, setLoading] = useState(false)
  const uploadHostel =async()=>{
    setLoading(true)
    const hostelData = new FormData()
    hostelData.append('name', name)
    hostelData.append('address',address)
    hostelData.append('contact',contact)
    hostelData.append('video',video[0])
    hostelData.append('email',email)
    hostelData.append('photo',image[0])
    hostelData.append('manager',jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access).user_id)
    

    const response = await fetch(`${SERVER_URL}getHostels/`,{
      method: 'POST',
      body: hostelData
    })
    if(parseInt(response.status) === 201){
      alert('Hostel info uploaded successfully')
      getMyHostel(setLoading)
    }else{
      alert('Uplaod failed, something went wrong')
    }
    setLoading(false)
  }
  return (
    <div className='container'>
    {loading ? displayLoader():
    <div className='mt-5 py-5 container d-flex flex-column align-items-center justify-content-center'>
      <h3 className='my-4 text-Db'>upload your hostel information</h3>
      <div className="d-lg-flex align-items-center">
        <Input
          type={'text'}
          field={'Hostel Name'}
          placeholder={'hostel name'}
          defaultvalue={name}
          Update={(e) => setname(e.target.value)}
        />
        <Input
          type={'email'}
          field={'Email'}
          placeholder={'hostel@email.com'}
          defaultvalue={email}
          Update={(e) => setemail(e.target.value)}
        />
      </div>
      <div className="d-lg-flex align-items-center align-items-center">
        <Input
          type={'text'}
          field={'Address'}
          placeholder={'1234 street'}
          defaultvalue={address}
          Update={(e) => setaddress(e.target.value)}
        />
        <Input
          type={'file'}
          field={'Display photo'}
          accept={'image/*'}
          Update={(e) => setimage(e.currentTarget.files)}
        />
        </div>

        <div className='d-lg-flex'>
        <Input
          type={'file'}
          field={'Display video'}
          accept={'video/*'}
          Update={(e) => setvideo(e.currentTarget.files)}
        />
        <Input
          type={'text'}
          field={'Contact'}
          placeholder={'must not exceed 14 characters'}
          defaultvalue={contact}
          Update={(e) => setcontact(e.target.value)}
        />
      </div>
      <button className="btn primary bg-blue px-5 text-light" onClick={uploadHostel}>
        submit
      </button>
    </div>
    }
    </div>
  )
}

export default M_Hostel
