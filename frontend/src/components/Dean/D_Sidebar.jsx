import React, { useContext } from 'react'
import { FaHome, FaHotel, FaSignOutAlt } from 'react-icons/fa'
import AuthContext from '../../utilities/AuthContext'
const D_Sidebar = () => {
  let {Logout} = useContext(AuthContext)
  return (
    <div className="bg-Db  sidebar d-flex align-items-center text-light" >
    <div className=" p-2 d-flex align-items-center rounded my-2" >
      <FaHome size={20} />
    </div>
    <FaHotel size={20} className='my-2' data-bs-toggle="modal" data-bs-target="#hostelsModal" type="button"  />
    <FaSignOutAlt size={20} className='my-2'  onClick={Logout}/>
  </div>
  )
}

export default D_Sidebar
