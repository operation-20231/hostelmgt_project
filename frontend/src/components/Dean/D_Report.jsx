import React, { useContext, useEffect, useState } from 'react'
import { FaUsers, FaHotel, FaCrown } from 'react-icons/fa'
import DataContext from '../../utilities/DataContext'
const D_Report = () => {
  let {Hostels, allUsers, getAllUsers, displayLoader} = useContext(DataContext)
  let [loading, setLoading] = useState(false)
  useEffect(()=>{
    getAllUsers(setLoading)
  },[])
  return (
  <>
    {
      loading ? displayLoader() : <div className='my-5 mx-3'>
      <p className='text-Db'>Report</p>
  
  
      <div className='d-flex report align-items-center'>
  
  
        <div className="bg-light px-3 py-4 text-center hair-line rounded">
          <FaUsers className='bg-red p-2 rounded' size={40} />
          <p className='my-2 text-Db'>{allUsers.filter(user=>user.groups[0] == 1).length} <span className="">Student(s)</span></p>
        </div>
  
   <a href="#managers" className='nav-link'>
   <div className="bg-light px-3 py-4 mx-1 text-center hair-line rounded">
          <FaCrown className='bg-red p-2 rounded' size={40} />
          <p className='my-2 text-Db'>{allUsers.filter(user=>user.groups[0] == 2).length} <span className="">Manager(s)</span></p>
        </div>
   </a>
  
  
        <div className="bg-light px-3 py-4 hair-line text-center mx-1 rounded" data-bs-toggle="modal" data-bs-target="#hostelsModal" type="button" >
          <FaHotel className='bg-light-blue p-2 rounded' size={40} />
          <p className='my-2 text-Db'>{Hostels.length} <span className="">Hostel(s)</span></p>
        </div>
  
  
      </div>
    </div>
    }
  </>
  )
}

export default D_Report
