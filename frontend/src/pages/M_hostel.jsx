import React, { useContext, useEffect, useState } from 'react'
import M_Navbar from '../components/manager/M_Navbar'
import AddRoom from '../components/AddRoom'
import H_View from '../components/HostelReview'
import Room from '../components/Room'
import Comments from '../components/Comments'
import M_Hostel from '../components/manager/M_Hostel'
import { Link, json } from 'react-router-dom'
import AuthContext from '../utilities/AuthContext'
import DataContext from '../utilities/DataContext'
import { jwtDecode } from 'jwt-decode'





const M_hostel = () => {
  let [loading, setLoading] = useState(true)
  let { SERVER_URL , Logout} = useContext(AuthContext)
  let { displayLoader, myhostel, setMyHostel } = useContext(DataContext)
  let { myrooms, getMyHostel } = useContext(DataContext)


  useEffect(() => {
    getMyHostel(setLoading)
  }, [])
  return (
    <>
      {

        JSON.parse(localStorage.getItem('authtokens')) ? jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access).groups[0] == "manager" ?
          loading ? displayLoader() : <div>
            <M_Navbar />
            {
              myhostel.id
                ?
                myhostel.status == "pending"
                  ? <div className="container text-center mt-5 pt-5">
                    <h3 className='text-center text-Db my-3'>Your hostel is stll under review</h3>
                    <small className='text-secondary text-center my-5'>Please be patient as the dean reviews your hostel, notifications will be sent <br />to your email incase you are approved</small>
                    <p className="text-center my-3 text-Db d-flex align-items-center justify-content-center">Leave page<button className='nav-link mx-2 bg-red text-light px-3 py-1 rounded-edges w-50' onClick={Logout}>Logout</button></p>
                  </div>
                  :
                  <div className="mt-5  pt-lg-5">
                    <H_View h_id={myhostel.id} />
                    <div className="my-5 container">
                      <h6 className='text-center text-Db'>My Rooms</h6>
                      <div className="row align-items-center justify-content-center">
                        {
                          myrooms.length > 0 ? myrooms.map(room => <Room key={room.id} room={room} />) : <p>No rooms found</p>
                        }
                      </div>
                    </div>
                    <Comments h_id={myhostel.id} />
                  </div>
                :
                <M_Hostel />
            }
            <AddRoom />
          </div>
          : <div className='text-center my-5 py-5 px-4 bg-db'><p className='text-light-blue'>You are not allowed here</p>
            <Link className='text-ok' to={'/'}>Back to home</Link>
          </div> : <div className='text-center my-5 py-5 px-4 bg-db'><p className='text-light-blue'>You are not allowed here</p>
          <Link className='text-ok' to={'/'}>Back to home</Link>
        </div>

      }
    </>
  )
}

export default M_hostel
