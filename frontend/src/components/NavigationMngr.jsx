import React from "react";
import { FaHome, FaHotel, FaUpload, FaRestroom, FaReply } from "react-icons/fa";
import Logo from '../assets/logo.png'
//declaring a functional component for defining UI components
//using JavaScript functions
const NavigationMngr = () => {
  //the following code is a JSX rendering
  return (
    <div className="topbar d-flex flex-column align-items-center">
      <div className="image d-flex flex-column align-items-center justify-content-center my-4">
        <img src={Logo} alt="" width={100}/>
      </div>
      <div className="icon text-center custom-shadow w-75 my-2 d-flex flex-column align-items-center justify-content-center py-3">
        <FaHome />
        <small>Home page</small>
      </div>
      <div
        type="button"
        data-bs-toggle="modal"
        data-bs-target="#exampleModal"
        data-bs-whatever="@mdo"
        className="icon text-center custom-shadow w-75 my-2 d-flex flex-column align-items-center justify-content-center py-3"
      >
        <FaUpload />
        <small>Upload Room</small>
      </div>
      <div className="icon text-center custom-shadow w-75 my-2 d-flex flex-column align-items-center justify-content-center py-3">
        <FaRestroom />
        <small>All Rooms</small>
      </div>
      <div className="icon text-center  custom-shadow w-75 my-2 d-flex flex-column align-items-center justify-content-center py-3">
        <FaReply />
        <small>Feedback</small>
      </div>
    </div>
  );
};

export default NavigationMngr;
