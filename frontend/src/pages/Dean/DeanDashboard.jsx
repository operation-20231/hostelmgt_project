import React, { useContext, useEffect, useState } from 'react'
import { FaHotel, FaAngleDown, FaBell, FaUsers } from 'react-icons/fa'
import D_Sidebar from '../../components/Dean/D_Sidebar'
import D_TopBar from '../../components/Dean/D_TopBar'
import D_Report from '../../components/Dean/D_Report'
import D_Manager from '../../components/Dean/D_Manager'
import D_hostels from '../../components/Dean/D_hostels'
import D_Requests from '../../components/Dean/D_Requests'
import { jwtDecode } from 'jwt-decode'
import { Link } from 'react-router-dom'
import DataContext from '../../utilities/DataContext'
import AuthContext from '../../utilities/AuthContext'
const DeanDashboard = () => {
  let {getAllUsers, displayLoader,allUsers} = useContext(DataContext)
  let [loading, setLoading] = useState(true)
  let {SERVER_URL} = useContext(AuthContext)
  const [width, setWidth] = useState(window.innerWidth)
  useEffect(()=>{
    getAllUsers(setLoading)
  },[])
  return (
    <>
    {
      loading ? displayLoader():JSON.parse(localStorage.getItem('authtokens')) ?  jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access).groups[0] == "dean" ?<>
      <div className='bg-Db fullpage d-lg-flex'>
     <div className={`bg-db h-100 m_section_1 dean`}>
       <div className="container  h-100 d-flex align-items-center ">
         <D_Sidebar />
         <div className='w-100  px-3 text-light'>
           <D_TopBar />
           <div className='hair-line custom-shadow text-Db my-5 py-4 px-3 rounded'>
             <p className="lead"><b>{jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access).first_name} {jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access).last_name}</b>, you are welcome aboard</p>
             <p className=''>Have a nice day</p>
           </div>
           <D_Report />
         </div>
       </div>
     </div>
     <div className={`text-light d-flex flex-column py-5 align-items-center m_section_2`}>
      <div id="dp" className="dean-profile p-4 rounded d-flex flex-column align-items-center justify-content-center">
      <div className="rounded-image rounded-circle">
         <img src={`${SERVER_URL}${jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access).photo_url}`} alt="" />
       </div>
       <p className='my-3'>{jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access).first_name} {jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access).last_name}</p>
      </div>
       <div className='my-4'>
         <p className='text-center managers opacity-50' id='managers'>Hostel managers</p>
         {
           allUsers.filter(user=>user.groups[0] == 2).length > 0 ? <div className="div px-5 managers" style={{overflowY: 'scroll', height: '50vh'}}>
           {
             allUsers.filter(user=>user.groups[0] == 2).map(user=><D_Manager user={user} />)
           }
         </div> : <p>No managers found</p>
         }
         
         
       </div>
     </div> <D_hostels/><D_Requests/>
   </div>
     </>:<div className='text-center my-5 py-5 px-4 bg-db'><p className='text-light-blue'>You are not allowed here</p>
     <Link className='text-ok' to={'/'}>Back to home</Link>
     </div>:<div className='text-center my-5 py-5 px-4 bg-db'><p className='text-light-blue'>You are not allowed here</p>
     <Link className='text-ok' to={'/'}>Back to home</Link>
     </div>
    }
    </>
   
  )
}

export default DeanDashboard
