import React, { useContext } from 'react'
import AuthContext from '../utilities/AuthContext'
import DataContext from '../utilities/DataContext';
import { jwtDecode } from 'jwt-decode';
import { useParams } from 'react-router-dom';
import {FaArrowAltCircleRight} from 'react-icons/fa'
const Room = ({ room }) => {
  let {currentRoom,setCurrentRoom} = useContext(DataContext)
  let { SERVER_URL } = useContext(AuthContext)
  let {q} = useParams
  function getRoomType() {
    switch (parseInt(room.capacity)) {
      case 1:
        return 'single room'
        break
      case 2:
        return 'Double room'
        break
      case 3:
        return 'tripple Room'
        break
      default:
        return 'invalid room type'
    }
  }
  function getSlotsRemaining() {
    let slots = parseInt(room.capacity) - parseInt(room.occupants.length)
    return slots.length > 0 || slots == 0 ? slots + ' slots available' : slots + ' slot available'
  }



  return (
    <div class="card custom-shadow col-lg-3 mx-1 my-3 px-0 border-0 text-light rounded bg-light">
      <div id={`carouselExample-${room.id}`} className="carousel slide">
        <div className="carousel-inner">
          {Object.keys(room)
            .filter((key) => key.startsWith("image") && room[key])
            .map((imageKey, index) => (
              <div
                key={index}
                className={`carousel-item ${index === 0 ? "active" : ""}`}
              >
                <img
                  src={SERVER_URL + room[imageKey]}
                  className="room-pic"
                  alt={`Room ${room.id} Image ${index + 1}`}
                />
              </div>
            ))}
        </div>
        <button
          className="carousel-control-prev"
          type="button"
          data-bs-target={`#carouselExample-${room.id}`}
          data-bs-slide="prev"
        >
          <span
            className="carousel-control-prev-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button
          className="carousel-control-next"
          type="button"
          data-bs-target={`#carouselExample-${room.id}`}
          data-bs-slide="next"
        >
          <span
            className="carousel-control-next-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>
      <div class="card-body px-4 d-flex flex-column justify-content-around align-items-start">
        <b class="card-title my-1 text-Db">{room.number}</b>
        <span class="card-text my-1 text-Db py-1">{getSlotsRemaining()}</span>

        <span class="card-text text-Db rounded-edges bold  my-1">UGX {room.price} </span>

        <span class="card-text my-1 text-secondary">{getRoomType()}</span>
        {
          jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access).groups[0] == "student" && <button onClick={()=>setCurrentRoom(room)} className="search-rs py-2   btn-primary  bg-Db room-btn border-0 btn rounded-edges mt-3`" data-bs-toggle="modal" data-bs-target="#exampleModal">
         <span className='d-flex align-items-center justify-content-between'>
         <span>Book now</span>
          <span><FaArrowAltCircleRight/></span>
         </span>
        </button> 
        }

      </div>
    </div>
  )
}

export default Room
