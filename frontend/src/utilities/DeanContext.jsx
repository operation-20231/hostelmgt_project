import { createContext, useState } from "react";
const DeanContext=createContext()
export const DeanProvider=({children})=>{
    const [hostels, setHostels] = useState([
        {
            id: 1,
            name: "Douglas Villa",
           address: "Kikoni",
           rating: 4,
           contact: '0766664546455',
           status:'pending',
            image: "https://images.pexels.com/photos/3659683/pexels-photo-3659683.jpeg?auto=compress&cs=tinysrgb&w=600"
        },
        {
            id: 2,
            name: "Olympia Hostel",
           address: "Kikoni",
           rating: 4,
           contact: '0766664546455',
           status:'active',
            image: 'https://images.pexels.com/photos/2952663/pexels-photo-2952663.jpeg?auto=compress&cs=tinysrgb&w=600'
        },
        {
            id: 3,
            name: "Nakiyinji",
           address: "Kikoni",
           rating: 4,
           contact: '0766664546455',
           status:'pending',
            image: 'https://images.pexels.com/photos/5379219/pexels-photo-5379219.jpeg?auto=compress&cs=tinysrgb&w=600'
        },
        {
            id: 4,
            name: "New Nana",
           address: "Kikoni",
           rating: 4,
           contact: '0766664546455',
           status:'active',
            image: 'https://images.pexels.com/photos/1115225/pexels-photo-1115225.jpeg?auto=compress&cs=tinysrgb&w=600'
        },
        {
            id: 5,
            name: "Kann Hostel",
           address: "Kikoni",
           rating: 4,
           contact: '0766664546455',
           status:'active',
            image: 'https://images.pexels.com/photos/2957461/pexels-photo-2957461.jpeg?auto=compress&cs=tinysrgb&w=600'
        },
        {
            id: 6,
            name: "Dream World",
           address: "Kikoni",
           rating: 4,
           contact: '0766664546455',
           status:'pending',
            image: 'https://images.pexels.com/photos/1771832/pexels-photo-1771832.jpeg?auto=compress&cs=tinysrgb&w=600'
        }])
    return <DeanContext.Provider value={{hostels, setHostels}}>
        {children}
    </DeanContext.Provider>
}

export default DeanContext
