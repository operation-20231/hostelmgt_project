import { useContext } from "react";
import Input from "../components/Input";
import AuthContext from "../utilities/AuthContext";
import { useLocation, useNavigate, Link } from "react-router-dom";
import { useEffect, useLayoutEffect } from "react";
import DataContext from "../utilities/DataContext";
import { jwtDecode } from "jwt-decode";

const Login = () => {
  let {message, setmessage} = useContext(DataContext)
  let {
    isAuthenticated,
    username,
    password,
    signin,
    Redirect,
    setUsername,
    setPassword,
  } = useContext(AuthContext);

  useLayoutEffect(()=>{
    if(localStorage.getItem('authtokens')){
      Redirect(jwtDecode(JSON.parse(localStorage.getItem("authtokens")).access).groups[0])
    }
  },[])
  return (
    <div className="fullpage d-flex align-items-center justify-content-center bg-light" >
      <div className="container login rounded py-5 d-flex flex-column justify-content-between align-items-center bg-light custom-shadow login-c">
        <p className="text-center text-Db">HOSTEL <span className="text-red">BOOKING </span>SYSTEM</p>
        <Input
          type={"email"}
          placeholder={"Enter your email"}
          field={"Email"}
          name={"username"}
          Update={(e) => setUsername(e.target.value)}
          defaultvalue={username}
        />
        <Input
          type={"password"}
          placeholder={"Enter your password"}
          field={"Password"}
          name={"password"}
          Update={(e) => setPassword(e.target.value)}
          defaultvalue={password}
        />
        <button
          className="btn btn-danger bg-blue border-0 px-4 py-2 text-light"
          onClick={signin}
          // style={{width:400}}
        >Sign in</button>
        <p className="text-center my-3 text-blue">
          <u>
            <small onClick={()=>setmessage('please check your email for your login credentials')}>Forgot password</small>
          </u>
        </p>
        <p className="text-center d-flex justify-content-center">
          <span className="d-none text-Db d-lg-block">Don't have an account ?,</span>
          <span className="signup">
            <Link to="/signup" className="nav-link text-primary">
              &nbsp;<u>Sign up</u>
            </Link>
          </span>
        </p>
      </div>
    </div>
  );
};

export default Login;
