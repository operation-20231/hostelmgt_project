import React from "react";
//this declares a functional component that takes a prop named "reservation"
const BookingMngr = ({ reservation }) => {
  return (
    <div className="py-3 px-4 row">
      <small className="col purple">{reservation.student}</small>
      <small className="col">{reservation.room}</small>
      <small className="col text-secondary">{reservation.date}</small>
      <small className="col">UGX {reservation.amount}</small>
    </div>
  );
};

export default BookingMngr;
