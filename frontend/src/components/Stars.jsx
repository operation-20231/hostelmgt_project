import React from 'react'
import { FaStar } from 'react-icons/fa'
const Stars = ({rating, reviews}) => {

  return (
    <span className="d-flex align-items-center">
        <small className="stars d-flex align-items-center justify-content-between text-secondary">
          {[...Array(5)].map((_, index) => (
            <FaStar
              key={index}
              className={
                index < Math.round(rating) ? "text-warning" : ""
              }
            />
          ))}
        </small>&nbsp; {reviews && `(${reviews} ${reviews > 1 ? 'reviews': reviews ==0 ? 'reviews': 'review'})`}
        </span>
  )
}

export default Stars
