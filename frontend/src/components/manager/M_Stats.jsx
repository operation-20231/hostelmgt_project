import React, { useContext, useEffect, useState } from 'react'
import DataContext from '../../utilities/DataContext'

const M_Stats = () => {
  const [laoding, setLoading] = useState(false)
  const {myrooms}=useContext(DataContext)
  return (
    <div className="m-stat my-lg-0 my-4 container row gap-1 align-items-start justify-content-end">
      <h6 className="text-center text-red">Room Statistics</h6>
        <div className="col-5 my-lg-0 py-2 px-3 bg-red text-light rounded">
            <span className="lead">{myrooms.filter(myroom=>myroom.capacity-myroom.occupants.length==0).length}</span><br />
            <small className="opacity-75">Full</small>
        </div>
        <div className="col-5 col-6 py-2 px-3 bg-Db rounded">
            <span className="lead">{myrooms.filter(myroom=>myroom.capacity-myroom.occupants.length!==0).length}</span><br />
            <small className=" opacity-75">Not full</small>
        </div>
        <div className="col-6 py-2 px-3 bg-db rounded hair-line">
            <span className="lead text-Db">{myrooms.length}</span><br />
            <small className=" text-secondary">Total </small>
        </div>
        <div className="col-5 py-2 col-3 px-3 bg-db rounded hair-line">
            <span className="lead text-Db">{myrooms.filter(myroom=>myroom.occupants.length==0).length}</span><br />
            <small className=" text-secondary">Empty</small>
        </div>
        
    </div>
  )
}

export default M_Stats
