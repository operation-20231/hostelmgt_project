import React, { useContext } from "react";
import DataContext from '../utilities/DataContext'
//declaring a functional component using a Javascript function
const BookingStatisticsMngr = () => {
  //we are using the useContext hook to access data about "rooms"
//from the DataContext
  let { rooms } = useContext(DataContext);
  return (
    <div className="px-3 py-3">
      <p>Accomodation</p>
      
      <div className="d-flex align-items-center ">
        
        <div className="statistics p-2 rounded">
          <small>Total</small>

          <p>{rooms.length}</p>
        </div>
      
        <div className="statistics p-2 rounded">
          <small>Fully Booked</small>
          <p>
            {
              rooms.filter((room) => room.capacity - (room.occupants).length === 0)
                .length
                //this code is checking the difference between the 'capacity' and 'occupants'
                //where it filters rooms where the number of occupants is equal to 0
            }
          </p>
        </div>
        <div className="statistics p-2 rounded">
          <small>Partially</small>
          <p>
            {
              rooms.filter(
                (room) =>
                  room.capacity - (room.occupants).length !== 0 && (room.occupants).length !== 0
                  //here we filter the room by checking if the difference is not 0
                  ).length
            }
          </p>
        </div>
        <div className="statistics p-2 rounded">
          <small>Empty</small>
          
          <p>{rooms.filter((room) => (room.occupants).length=== 0).length}
          
          </p>
            
        </div>
      </div>
    </div>
    //"{rooms.filter((room) => room.occupants === 0).length}" will check if the difference is equal to 0 
    //then find out the number of rooms
    //"<p>{rooms.length}</p>" is the array that represents the total number of rooms
  );
};

export default BookingStatisticsMngr;
