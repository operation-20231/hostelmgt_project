const Input = ({ field, type, placeholder, name, Update, defaultvalue,accept , min}) => {
  return (
    <div className="mx-3 my-1">
      <label className="form-label text-left text-Db">
        <small className="mx-2">{field}</small>
      </label>
      <br />
      <input
        type={type}
        className="my-2 px-3 py-2 bg-0 rounded text-Db" id="input"
        placeholder={placeholder}
        name={name}
        onChange={Update}
        accept={accept}
        min={min}
        value={defaultvalue}
      />
    </div>
  );
};

export default Input;
