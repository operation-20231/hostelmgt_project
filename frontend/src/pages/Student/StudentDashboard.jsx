import { useContext, useEffect } from "react";
import Comments from "../../components/Comments";
import HostelList from "../../components/HostelList";
import Navbar from "../../components/Navbar";
import Sidebar from "../../components/Sidebar";
import HostelReview from "../../components/HostelReview";
import AddCmment from '../../components/AddComment'

const StudentDashboard = () => {
  useEffect(()=>{
    window.scrollTo(0, 0);
  },[])
  return (
    <>
      <div className="fixed-top shadow-lg">
      <Navbar />
        <Sidebar />
      </div>
      <div className="container mt-5 pt-0 pt-lg-5  d-flex flex-column">
         <div className="mt-5">
         <HostelReview />
         </div>

          </div>
          <HostelList />
        <Comments />
        <AddCmment/>
      <div className="text-center py-3 bg-Db">
        All rights reserved &copy;operation2023
      </div>
    </>
  );
};

export default StudentDashboard;
