import React from "react";
//importing the icon 'FaSearch' from the 'react-icons/fa' module
import { FaSearch } from "react-icons/fa";
//declaring a functional component
const SearchRoomMngr = () => {
  return (
    <div
      className="search-room w-75 mx-2 opacity-50 d-flex justify-content-between align-items-center py-2 px-4"
      
    >
      <input type="text"/>
      <FaSearch />
    </div>
  );
};

export default SearchRoomMngr;
