import { createContext, useState } from "react";
import { useNavigate } from "react-router-dom";

const UploadContext = createContext({});

export const UploadProvider = ({ children }) => {
    let SERVER_URL = 'https://kigongovincent.pythonanywhere.com'
    let navigate=useNavigate()
  // room upload
  const [number, setNumber] = useState('');
  const [capacity, setCapacity] = useState('');
  const [price, setPrice] = useState('');
  const [imageUploads, setImageUploads] = useState([]);

  function uploadRoom(id) {
    
      let uploadData = new FormData();
      for (let i = 0; i < imageUploads.length; i++) {
        uploadData.append(`image${i}`, imageUploads[i]);
      }
      uploadData.append("number", number);
      uploadData.append("capacity", capacity);
      uploadData.append("price", price);
      uploadData.append('hostel', id)
    //   console.log(capacity, price, number, imageUploads)
      fetch(`${SERVER_URL}/getRooms/${id}`,{
        method: 'POST',
        body: uploadData
      })
      .then(res=>{
        if(res.status == 201){
          alert('Uploaded successfully')
          navigate('/manager/dashboard')
        }
        else{
          alert('something went wrong')
        }
      })
      
  }

  return (
    <UploadContext.Provider
      value={{
        number,
        capacity,
        price,
        imageUploads,
        uploadRoom,
        setCapacity,
        setPrice,
        setImageUploads,
        setNumber,
      }}
    >
      {children}
    </UploadContext.Provider>
  );
};
export default UploadContext;
