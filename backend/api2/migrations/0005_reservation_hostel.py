# Generated by Django 4.2.5 on 2023-11-23 22:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api2', '0004_remove_reservation_paymentid_reservation_amount'),
    ]

    operations = [
        migrations.AddField(
            model_name='reservation',
            name='hostel',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='api2.hostel'),
        ),
    ]
