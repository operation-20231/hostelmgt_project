import React, { useContext } from 'react'
import { FaAngleDown, FaBell } from 'react-icons/fa'
import moment from 'moment'
import DataContext from '../../utilities/DataContext'
const D_TopBar = () => {
  let now = new Date()
  const {Hostels} = useContext(DataContext)
  function getRequests(){
    if(Hostels){
      return Hostels.filter(hostel=>hostel.status === "pending").length
    }
    else{
      return 0
    }
  }
  return (
    <div className="container text-Db  d-lg-flex align-items-end justify-content-between">
    <div className='d-non d-lg-block'>
      <h5 className='text-red'>Dashboard</h5>
      <span>{moment(now).calendar()}</span>
    </div>
    <div className='d-flex  my-4 my-lg-0 align-items-end'>
      <span className="text-light-blue" data-bs-toggle="modal" data-bs-target="#RequestsModal" type="button"><span className='d-flex align-items-center'>View <span className="d-none d-lg-block">&nbsp; Requests </span><FaAngleDown /></span></span>
      <span  className=" bg-Db text-light py-2 px-lg-5 px-3 mx-2 rounded " data-bs-toggle="modal" data-bs-target="#RequestsModal" type="button">{getRequests()} request(s) <FaBell /></span>
    </div>
  </div>
  )
}

export default D_TopBar
