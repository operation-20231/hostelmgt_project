import { useContext, useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import DataContext from "../utilities/DataContext";
import AuthContext from "../utilities/AuthContext";
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import Stars from "./Stars";
import { FaBlenderPhone, FaPhone } from "react-icons/fa";
const HostelReview = ({ h_id }) => {
  const [coordinates, setCoordinates] = useState(null);
  const handleLocationSubmit = async (address) => {
    setLoading(true)
    try {
      const response = await fetch(
        `https://nominatim.openstreetmap.org/search?format=json&q=${encodeURIComponent(
          address
        )}&accept-language=en`
      );

      if (!response.ok) {
        throw new Error('Failed to fetch location data');
      }

      const data = await response.json();

      if (data.length > 0) {
        const { lat, lon } = data[0];
        setCoordinates({ lat: parseFloat(lat), lon: parseFloat(lon) });
      } else {
        console.log('Location not found');
      }
    } catch (error) {
      console.error('Error:', error);
    }
    setLoading(false)
  }

  let [currentHostel, setCurrentHostel] = useState();
  let [loading, setLoading] = useState(true);
  let navigate = useNavigate()
  let { id } = useParams();
  if (h_id) {
    id = h_id
  }
  let { Hostels, rooms, displayLoader } = useContext(DataContext);
  let { SERVER_URL } = useContext(AuthContext)


  useEffect(() => {
    fetch(`${SERVER_URL}/Hostel/${id}`, {
      headers: {
        'Authorization': `Bearer ${JSON.parse(localStorage.getItem('authtokens')).access}`
      }
    })
      .then(res => res.json())
      .then(data => {
        setCurrentHostel(data)
        handleLocationSubmit(data.address, setLoading)
      })
    setLoading(false);

  }, []);
  return (
    <>
      {loading ? (
        displayLoader()
      ) : (
        <>
          {
            currentHostel && <div className="my-lg-1 mt-4 ">
  <div className="d-lg-flex justify-content-center align-items-center hostel">
  <img src={SERVER_URL + currentHostel.photo} className="  mx-lg-3 hostel-pic rounded" />
              <div className="details h-50">
              <div className="bg-Db px-4 rounded  my-lg-2 my-3 py-2">
                <h1 style={{ fontWeight: 'lighter',textTransform: 'uppercase' }}>{currentHostel.name}</h1>
                <p className="opacity-75"><span >Located, </span> {currentHostel.address}</p>
                <p className="opacity-75"><span ><FaPhone/></span> <span className="">{currentHostel.contact}</span></p>
                <span className="my-5"><Stars rating={currentHostel.rating} /></span>
              </div>
              <p className=" text-center rounded-edges d-block d-lg-none bg-red px-4  py-2  my-3">Hostel Geo Location</p>
              {coordinates ? (
       <div className="map">
         <MapContainer center={coordinates} zoom={9} style={{ height: '180px', width: '100%' ,borderRadius: 10}} className="hair-line">
          <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          />
          <Marker position={coordinates}>
          </Marker>
        </MapContainer>
       </div>
      ) : <p>Location Not found</p>}
              </div>
  </div>
            </div>
          }
            
        </>
      )}
    
    </>
  );
};

export default HostelReview;
