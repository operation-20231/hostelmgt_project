import React, { useState, useEffect, useContext } from "react";
import { useParams } from "react-router-dom";
import DataContext from "../utilities/DataContext";
import { jwtDecode } from "jwt-decode";
import Room from "./Room";
const HostelList = ({h_id}) => {
  const [loading, setLoading] = useState(false);
  let {id} = useParams()
  if(h_id){
    id = h_id
  }
  let {rooms, getRooms, displayLoader} = useContext(DataContext)
  useEffect(()=>{
    setLoading(true)
getRooms(id)
setLoading(false)
  },[])
  return (
<>
{
  loading ?displayLoader():<>
      <div className=" bg-lighter mt-4">
        <div className="container py-5">
        <p className="text-center opacity-75 text-Db">Available room(s) <span className="bg-red text-light px-2 py-1 mx-2 rounded">{rooms.filter(room =>room.capacity-(room.occupants).length  > 0 && !room.occupants.includes(jwtDecode((JSON.parse(localStorage.getItem('authtokens')).access)).user_id)).length}</span></p>
        <div className="row align-items-center justify-content-center">
        {rooms && rooms.map((room) => (
   room.capacity-(room.occupants).length  > 0 && !room.occupants.includes(jwtDecode((JSON.parse(localStorage.getItem('authtokens')).access)).user_id)? <Room key={room.id} room={room}/>: ''
        ))}
        </div>
        </div>
      </div>
    </>
}
</>
  );
};

export default HostelList;
