import React, { useContext } from 'react'
import D_hostel from './D_hostel'
import DataContext from '../../utilities/DataContext'
const D_hostels = () => {
  const {Hostels} = useContext(DataContext)
  return (
    <div class="modal fade" id="hostelsModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-fullscreen text-light">
    <div class="modal-content blurred p-1 ">
      <div class="modal-header border-0">

        <p class="modal-title fs-5" id="exampleModalLabel">All hostels</p>
        <button type="button" class="btn-close bg-lighter" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body row justify-content-center align-items-center gap-2">
        {
          Hostels ? Hostels.filter(hostel=>hostel.status=="active").length > 0 ? Hostels.filter(hostel=>hostel.status=="active").map(hostel=>(<D_hostel hostel={hostel} key={hostel.id}/>
          )) :<div className='text-center'><h1>No Hostels found</h1><p className='my-3'>Back to <span className="px-3 py-2 bg-light text-Db rounded-edges" data-bs-dismiss="modal" type="button">dashboard</span></p></div> : <div className='text-center'><h1>No Hostels found</h1><p className='my-3'>Back to <span className="px-3 py-2 bg-light text-Db rounded-edges" data-bs-dismiss="modal" type="button">dashboard</span></p></div>
        }
      </div>
    </div>
  </div>
</div>
  )
}

export default D_hostels
