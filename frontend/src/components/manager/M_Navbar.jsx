import React, { useContext } from 'react'
import { FaHome, FaUpload, FaHotel, FaComment, FaSignOutAlt, FaTools } from 'react-icons/fa'
import { Link } from 'react-router-dom'
import AuthContext from '../../utilities/AuthContext'
import DataContext from '../../utilities/DataContext'
const M_Navbar = () => {
  const {Logout} = useContext(AuthContext)
  let {myhostel} = useContext(DataContext)
  return (
    <nav className="navbar fixed-top navbar-expand-lg shadow-md d navbar-dark bg-Db">
        <div className="container">
            <Link className='navbar-brand'><span className="">Hostel Booking System</span></Link>
            <button className="navbar-toggler" style={{width: '15vw'}} data-bs-target="#navmenu" data-bs-toggle="collapse" type='button'>
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navmenu">
                <ul className="ms-auto navbar-nav" >
                    <li className="nav-item"><Link to={'/manager/dashboard'} className='nav-link d-flex align-items-center'><FaHome className="m-2 " size={17} /><span className="d-none d-lg-block"/>Home</Link></li>
                    {
                      myhostel.status == "active" && <li className="nav-item"><Link className='nav-link d-flex align-items-center' to={'/manager/myhostel'}><FaHotel className="m-2 " size={17} /><span className="d-none d-lg-block"/>My Hostel</Link></li>
                    }
                   {
                    myhostel.status == "active" &&  <li className="nav-item"><Link className='nav-link d-flex align-items-center' data-bs-toggle="modal" data-bs-target="#addroom" type="button"><FaUpload className="m-2" size={17} /><span className="d-none d-lg-block"/>Upload Room</Link></li>
                   }
                    <li className="nav-item" onClick={Logout}><Link className='nav-link d-flex align-items-center'><FaSignOutAlt className="m-2 " size={17} /><span className="d-none d-lg-block"/>Signout</Link></li>
                </ul>
            </div>
        </div>
    </nav>
  )
}

export default M_Navbar
