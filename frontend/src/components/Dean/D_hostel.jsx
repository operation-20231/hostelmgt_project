import React, { useContext , useState, useEffect} from 'react'
import AuthContext from '../../utilities/AuthContext'
import Stars from '../Stars'
const D_hostel = ({hostel}) => {

    const {SERVER_URL} = useContext(AuthContext)
    let [comment1, setComment1] = useState([])
    useEffect(()=>{
      fetch(`${SERVER_URL}/feedback/${hostel.id}`)
        .then((res) => res.json())
        .then((data) => setComment1(data));
  
    },[])
  
    function reviewCount() {
      return comment1.length
    }
  
    return (
        <div className='col-lg-3 card bg-Db text-light p-0 border-0 '>
            <div className="card-body p-0"> <img src={SERVER_URL+hostel.photo} alt="" className="card-img-top d-image"/>
            
            <div className="px-4 py-1">
            <p className="card-title">{hostel.name}</p><p className='opacity-75'>{hostel.address}</p><p><Stars reviews={reviewCount()} rating={hostel.rating} /></p>
                </div></div>
        </div>
    )
}

export default D_hostel

