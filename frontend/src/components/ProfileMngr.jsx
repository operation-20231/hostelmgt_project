import React, { useContext } from "react";
import { FaPhone, FaEnvelope } from "react-icons/fa";
//importing "AuthContext" a module related to authetication located the path
//defined below

//declaring a functional component to define UI components using a JavaScript function
const ProfileMngr = ({user, SERVER_URL}) => {

  return (
    <div

      className=" d-flex flex-column align-items-center justify-content-between py-5 profile bg-lighter rounded custom-shadow"
    >
      
      <div className="p-image d-flex align-items-center justify-content-center" >
        
        <img src={`${SERVER_URL}/${user.photo}`} alt=""/>

      
      </div>
      
      <small>{user.first_name} {user.last_name}</small>
      <small className="text-secondary">
        <FaPhone className="mx-2 purple" />
        {user.contact}
      </small>
      <small className="text-secondary">
        <FaEnvelope className="mx-2 purple" />
        {user.email}
      </small>
    </div>
    //{account.photo}' specifies the source Url for the image
    //'{account.email}' specifies the source Url for the email
  //"{account.contact}" specifies the source URL for contact
    );
};

export default ProfileMngr;
