import React, { useContext } from "react";
import DataContext from "../utilities/DataContext";
const HostelCardMngr = ({hostelinfo, SERVER_URL}) => {
  let { hostel } = useContext(DataContext);
  return (
    <div>
      <div className="opacity-50 py-1 px-3 ">Hostel info</div>
      <div className="d-flex px-3">
        <img
          src={`${SERVER_URL}${hostel.photo}`}
          alt=""
          style={{ height: 140, marginRight: 5 }}
        />
        <video
          src={`${SERVER_URL}${hostel.video}`}
          autoPlay
          muted
          controls
          loop
          style={{ height: 140, marginRight: 5 }}
        />
      </div>
    </div>
  );
};

export default HostelCardMngr;
