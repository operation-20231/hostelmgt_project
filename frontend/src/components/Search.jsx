import React, { useContext, useEffect, useState } from "react";
import { BiSearch } from "react-icons/bi";
import DataContext from "../utilities/DataContext";
import { useNavigate } from "react-router-dom";
const Search = () => {
  const navigate = useNavigate();
  const { Hostels, setHostels } = useContext(DataContext);
  const [prevHostels, setPrevHostels] = useState(Hostels);
  const [search, setSearch] = useState("");
  return (
    <div className="search-bar px-3 rounded-edges d-flex bg-db-light py-1 my-1 align-items-center justify-content-between">
      <input
      style={{width: 200}}
        type="text"
        placeholder="Search for hostels..."
        value={search}
        onChange={(e) => setSearch(e.target.value)}
        list="datalist"
        className="bg-0 border-0 text-light"
      />
      <BiSearch />
    </div>
  );
};

export default Search;
