import React, { useContext } from "react";
import Sidebar from "../components/Sidebar";
import Navbar from "../components/Navbar";
import AllHostels from "../components/AllHostels";
import { Link } from "react-router-dom";
import AuthContext from "../utilities/AuthContext";
import { jwtDecode } from "jwt-decode";
const Hostels = () => {
  return (
    <>
      {
        
        JSON.parse(localStorage.getItem('authtokens')) ?  jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access).groups[0] == "student" ? <>
       <div className="fixed-top">
       <Navbar />
        <Sidebar />
       </div>
      <div className="container d-flex flex-column mt-5 pt-5">
        <h1 className="text-Db mt-5"><span className="e">E</span>xplore the most <span className="">comforting </span><br />And <span className="text-secondary">intuitive</span> hostels</h1>
        <span className="text-Db">At the most affordable prices</span>
        <AllHostels />
       
      </div>
       <div className="text-center py-2 hair-line text-secondary">
        All rights reserved &copy;operation2023
      </div>
        </>: <div className='text-center my-5 py-5 px-4 bg-db'><p className='text-light-blue'>You are not allowed here</p>
      <Link className='text-ok' to={'/'}>Back to home</Link>
      </div>:<div className='text-center my-5 py-5 px-4 bg-db'><p className='text-light-blue'>You are not allowed here</p>
      <Link className='text-ok' to={'/'}>Back to home</Link>
      </div>
       
      }
    </>
  );
};

export default Hostels;
