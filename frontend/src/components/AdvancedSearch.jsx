import React, { useContext, useState } from "react";
import { FaStar, FaFacebook, FaLinkedin, FaInstagram, FaSearch } from "react-icons/fa";
import DataContext from "../utilities/DataContext";
import AuthContext from "../utilities/AuthContext";
import Input from "./Input";
import { useNavigate } from "react-router-dom";
function AdvancedSearch() {
  let {SERVER_URL} = useContext(AuthContext)
  const [name, setname] = useState("")
  const [rating, setrating] = useState("")
  const [price, setprice] = useState("")
  const [address, setaddress] = useState("")
  const [capacity, setcapacity] = useState("")
  const {setSearchResults, displayLoader } = useContext(DataContext);
  const [loading, setLoading] = useState(false)
  let navigate = useNavigate()
  function getSearch(){
    setLoading(true)

    fetch(`${SERVER_URL}asearch/`,{
      method: 'POST',
      headers:{
        'Content-type': 'application/json'
      },
      body:JSON.stringify({
        name: name,
        price: price,
        rating: rating,
        address: address,
        capacity: capacity
      })
    })
    .then(res=>res.json())
    .then(data=>{
      setSearchResults(data)
    })
    setLoading(false)
    navigate('/search')
    
  }
  return (
    <div className="modal fade" id="AdvancedSearch" tabIndex="-1" >
      <div className="modal-dialog text-Db">
        {
          loading ? displayLoader() :<div className="modal-content bg-db">
          <div className="modal-header border-0 bg-Db text-light">

            <p className="modal-title fs-5">Advanced Search</p>
            <button type="button" className="btn-close bg-lighter" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div className="modal-body">
            <div className="container">
              <Input field={'Hostel'} type={'text'} placeholder={'Enter hostel name....'} defaultvalue={name} Update={(e)=>setname(e.target.value)}/>
              <Input field={'Rating'} type={'number'} placeholder={'allowed numbers 0 to 5'} defaultvalue={rating} Update={(e)=>setrating(e.target.value)}/>
              <Input field={'Price'} type={'number'} min={400000} placeholder={'UGX 200,000'} defaultvalue={price} Update={(e)=>setprice(e.target.value)}/>
              <Input field={'Address'} type={'text'} placeholder={'Makerere Kavule'} defaultvalue={address} Update={(e)=>setaddress(e.target.value)}/>
              <Input field={'Capacity'} type={'number'} placeholder={'1 person (single room)'} defaultvalue={capacity} Update={(e)=>setcapacity(e.target.value)}/>
              <button data-bs-dismiss="modal" aria-label="Close" onClick={getSearch} className="btn btn-danger bg-red text-db border-0 as-btn px-3 rounded mx-3 d-flex align-items-center justify-content-center"><FaSearch className="mx-1"/>Perform search</button>

            </div>
          </div>
        </div>
        }
      </div>

    </div>
  );
}

export default AdvancedSearch;
