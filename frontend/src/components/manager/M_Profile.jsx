import React, { useContext } from 'react'
import {jwtDecode} from 'jwt-decode'
import { FaPhoneSlash, FaEnvelope } from 'react-icons/fa'
import AuthContext from '../../utilities/AuthContext'
const M_Profile = () => {
    let {SERVER_URL} = useContext(AuthContext)
    let manager =  jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access)
    return (
        <div  className="px-lg-5 px-0 custom-shadow d-flex align-items-center justify-content-around flex-column pt-2 rounded">
            <div className="rounded-image rounded-circle my-2">
                <img src={ `${SERVER_URL}${manager.photo_url}`} alt="" />
            </div>
            <h6 className='text-Db'>{manager.first_name}{manager.last_name}</h6>

            <p className='text-Db'><FaEnvelope className='mx-1 text-Db' />{manager.email}</p>
            <p className='text-secondary'><FaPhoneSlash className='mx-1 ' />{manager.contact}</p>
        </div>
    )
}

export default M_Profile
