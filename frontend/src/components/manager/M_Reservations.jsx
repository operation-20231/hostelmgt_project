import React, { useContext, useEffect, useState } from 'react'
import AuthContext from '../../utilities/AuthContext'
import DataContext from '../../utilities/DataContext'
import moment from 'moment'
const getMyReservation=async(SERVER_URL, setReservationz, setLoading, hostel)=>{
    setLoading(true)
    const response = await fetch(`${SERVER_URL}getReservations/${hostel}`)
    const data = await response.json()
    if(response.status == 200){
        setReservationz(data)
        setLoading(false)
    }else{
        setLoading(false)
    }
    setLoading(false)
}

const M_Reservations = () => {
    const [loading, setLoading] = useState(true)
    let [reservationz, setReservationz] = useState([])

    let {SERVER_URL} = useContext(AuthContext)
    let {displayLoader, myhostel} = useContext(DataContext)
    function getRTime(timestamp) {
        const datetime = moment(timestamp);
        const relativeTime = moment(timestamp).fromNow()
        return relativeTime
      }
    useEffect(()=>{
if(myhostel){
    getMyReservation(SERVER_URL,setReservationz, setLoading, myhostel.id)
}
setLoading(false)
    }, [])
    return (
       <>
       {
        loading? displayLoader(): reservationz.length > 0 ?
        <div>
        <p className=" text-center ">Booking history</p>
        <div class="list-group o-x" style={{ maxHeight: window.innerHeight / 2, overflowY: 'scroll' }}>
            <span class="list-group-item bg-Db border-0 row text-light br-0 d-flex align-items-center justify-content-between" aria-current="true">
                <div className='col-2  text-light'>Student</div>
                <div className='col-2 d-none d-lg-block text-light'>Amount</div>
                <div className='col-6  col-lg-2  text-light' style={{textAlign: 'end'}}>Room Number</div>
                <div className='col-2  d-none d-lg-block text-light'>Payment Date</div>
            </span>
            {
                reservationz.length >0 ? reservationz.map(i => (
                    <span class="list-group-item text-Db  row d-flex align-items-center justify-content-between">
                        <div className='col-2 flex-wrap'>{i.email.length > 20 ? i.email.slice(0,5)+'...'+ i.email.slice(10,20) : i.email}</div>
                        <div className='col-2 d-none d-lg-block text-secondary' >{i.amount}</div>
                        <div className='col-2  text-secondary' >{i.number}</div>
                        <div className="col-2 d-none d-lg-block">{getRTime(i.created)}</div>
                    </span>
                )):     <p>No reservations</p>
            }


        </div>
    </div>: <h2 className='text-Db text-center my-5'>There are no reservations</h2>
       }
       </>
    )
}

export default M_Reservations
