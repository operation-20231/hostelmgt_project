import React, { useContext } from 'react'
import AuthContext from '../../utilities/AuthContext'
import DataContext from '../../utilities/DataContext'

const D_Manager = ({user}) => {
    let {SERVER_URL} = useContext(AuthContext)
    let {Hostels} = useContext(DataContext)
    return (
        <div className='d-flex align-items-center my-4'>
            <div className="small-rounded-image rounded-circle" >
                {
                    user.photo && <img src={`${SERVER_URL}${user.photo}`}/>
                }
            </div>
            <div>
                <small className="">{user.email.length > 12 ? user.email.slice(0,20)+'...' : user.email}</small><br />
                <small className='opacity-50'>{Hostels.filter(hostel => hostel.manager == user.id)[0] && Hostels.filter(hostel => hostel.manager == user.id)[0].name}</small>
            </div>
        </div>
    )
}

export default D_Manager
