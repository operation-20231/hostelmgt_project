import React, { useContext, useEffect } from "react";
import Login from "./pages/Login";
import Home from "./pages/Student/StudentDashboard";
import Hostels from "./pages/Hostels";
import Profile from "./pages/Profile";
import Reservation from "./pages/Reservation";
import { DataProvider } from "./utilities/DataContext";
import { AuthProvider } from "./utilities/AuthContext";
import Paypal from './components/Paypal'
import { Routes, Route, useNavigate } from "react-router-dom";
import Signup from "./pages/Signup";
import ManagerHomePage from "./pages/ManagerHomePage";
import DeanDashboard from './pages/Dean/DeanDashboard'
import AdvancedSearch from "./components/AdvancedSearch";
import M_hostel from "./pages/M_hostel";
import Notify from "./components/Notify";
import RSearch from './pages/RSearch'
const App = () => {
  return (
    <>
    <DataProvider>
      
        <AuthProvider>
          <Routes>
          <Route index element={<Login/>}/>
            <Route path="/manager">
              <Route index element={<Login/>}/>
              <Route path="/manager/dashboard" element={<M_hostel/>}/>  
              <Route path="/manager/myhostel" element={<ManagerHomePage/>}/>
            </Route>

          <Route path="/dean/dashboard" element={<DeanDashboard/>}/>
            
            <Route path="/signup" element={<Signup />} />
            <Route path="/login" element={<Login />} />
            <Route path="/search" element={<RSearch />} />
            
            <Route path="/hostel/:id" element={<Home />} />
            <Route path="/student/dashboard" element={<Hostels />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/reservation" element={<Reservation />} />
             
            
          </Routes>
          <Paypal/>
        <Notify/>

          <AdvancedSearch/>
          
        </AuthProvider>
      </DataProvider>
    </>
  );
};

export default App;
