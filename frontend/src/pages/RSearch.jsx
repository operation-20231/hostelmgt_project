import React, { useContext, useEffect, useState } from 'react'
import Navbar from '../components/Navbar'
import DataContext from '../utilities/DataContext'
import Sidebar from '../components/Sidebar'
import Room from '../components/Room'
import { jwtDecode } from 'jwt-decode'
const RSearch = () => {
  let { searchResults, displayLoader } = useContext(DataContext)
  const [res, setRes] = useState('No results found')
  const [loading, setLoading] = useState(true)
  useEffect(() => {
    setLoading(false)
  }, [])
  return (
    <>
    <div className="fixed-top">
      
    <Navbar />
        <Sidebar />
    </div>
      <div className='my-5 pt-5 container'>
        <div className="row container pt-3">
          {
            loading ? displayLoader() : <>
              {
                searchResults.length > 0 ?
                  searchResults.map(room => room.capacity - (room.occupants).length > 0 && !room.occupants.includes(jwtDecode((JSON.parse(localStorage.getItem('authtokens')).access)).user_id) && <Room key={room.id} room={room} />) ? searchResults.map(room => room.capacity - (room.occupants).length > 0 && !room.occupants.includes(jwtDecode((JSON.parse(localStorage.getItem('authtokens')).access)).user_id) && <Room key={room.id} room={room} />) : 'hey' :
                  <p className='text-center text-light-blue my-5'>{res}</p>
              }
            </>
          }
        </div>
      </div>
    </>
  )
}

export default RSearch
