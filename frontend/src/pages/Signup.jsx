import React, { useContext } from "react";
import Input from "../components/Input";
import { Link } from "react-router-dom";
import AuthContext from "../utilities/AuthContext";
const Signup = () => {
  let {
    newAddress,
    newContact,
    newEmail,
    newPassword1,
    newPassword2,
    setLast_name,
    setFirst_name,
    first_name,
    last_name,
    setNewAddress,
    setNewEmail,
    setNewContact,
    setNewPassword1,
    setNewPassword2,
    signup,
    setNewProfilePicture,
    accountType,
    setAccountType
  } = useContext(AuthContext);
  return (
    <div className=" text-light d-flex align-items-center justify-content-center">
      <div className=" py-5 d-flex flex-column justify-content-between align-items-center ">
        <p className="text-center text-Db">HOSTEL <span className="text-red">MANAGEMENT </span>SYSTEM</p>
        <p className="text-center text-light-blue">Sign up form</p>
        <div className="d-lg-flex">
          <Input
            type={"text"}
            field={"First name"}
            Update={(e) => setFirst_name(e.target.value)}
            defaultvalue={first_name}
            placeholder={'ofwono'}
          />
          <Input
            type={"text"}
            field={"last_name"}
            Update={(e) => setLast_name(e.target.value)}
            defaultvalue={last_name}
            placeholder={'daniel'}
          />
        </div>
        <div className="d-lg-flex align-items-center">
          <Input
            type={"text"}
            field={"Address"}
            Update={(e) => setNewAddress(e.target.value)}
            defaultvalue={newAddress}
            placeholder={'kikoni'}
          />
          <Input
            type={"number"}
            field={"Contact"}
            Update={(e) => setNewContact(e.target.value)}
            defaultvalue={newContact}
            placeholder={'65756478468'}
          />
        </div>
        <div className="d-lg-flex">
          <Input
            type={"email"}
            field={"Email"}
            Update={(e) => setNewEmail(e.target.value)}
            defaultvalue={newEmail}
            placeholder={'of@email.com'}
          />
          <Input
            type={"file"}
            accept={'image/*'}
            field={"Select profile picture"}
            Update={(e) => setNewProfilePicture(e.currentTarget.files)}
          />
        </div>
        <div className="d-lg-flex align-items-center">
          <Input
            type={"password"}
            placeholder={"Enter your password"}
            field={"Password"}
            name={"password"}
            Update={(e) => setNewPassword1(e.target.value)}
            defaultvalue={newPassword1}
          />
          <Input
            type={"password"}
            placeholder={"Enter password again"}
            field={"Password confirmation"}
            name={"password"}
            Update={(e) => setNewPassword2(e.target.value)}
            defaultvalue={newPassword2}
          />
        </div>

  
          <select className="form-control w-75 my-3 text-center" value={accountType} onChange={(e) => setAccountType(e.target.value)}>
            <option value="">Select an account type</option>
            <option value="student">Student</option>
            <option value="manager">Manager</option>
          </select>
        <input
          className="btn p-2 btn-danger bg-Db border-0 px-4 text-light"
          value="Sign up"
          type="submit"
          onClick={signup}
        />

        <p className="text-center text-Db my-3 d-flex align-items-center justify-content-center">
          Have an account,{" "}
          <span className="signup">
            <Link to="/login" className="nav-link text-primary">
              &nbsp;Sign in
            </Link>
          </span>
        </p>
      </div>
    </div>
  );
};

export default Signup;
