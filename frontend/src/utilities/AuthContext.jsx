import { createContext, useState, useEffect, useContext } from "react";
import { useNavigate, Link } from "react-router-dom";
import { jwtDecode } from 'jwt-decode'
import DataContext from "./DataContext";

const AuthContext = createContext({});


export const AuthProvider = ({ children }) => {
  let SERVER_URL = 'https://kigongovincent.pythonanywhere.com/'
  let { displayLoader } = useContext(DataContext)
  const [loading, setLoading] = useState(false)
  const [AuthTokens, setAuthTokens] = useState(JSON.parse(localStorage.getItem('authtokens')))


  let navigate = useNavigate();


  const [user, setUser] = useState({});
  const getUser = async (id) => {
    const response = await fetch(`${SERVER_URL}getUser/${id}`)
    const data = await response.json()
    if (response.status == 200) {
      setUser(data)
    } else {
      alert('something went wrong')
    }
  }


  let [username, setUsername] = useState("");
  let [password, setPassword] = useState("");


  let [newProfilePicture, setNewProfilePicture] = useState([]);
  let [first_name, setFirst_name] = useState("");
  let [last_name, setLast_name] = useState("");
  let [newEmail, setNewEmail] = useState("");
  let [newAddress, setNewAddress] = useState("");
  let [newContact, setNewContact] = useState("");
  let [newPassword1, setNewPassword1] = useState("");
  let [newPassword2, setNewPassword2] = useState("");
  let [accountType, setAccountType] = useState('')

  let signup = async () => {
    setLoading(true)
    if (first_name && last_name) {
      if (newEmail) {
        if (newContact) {
          if (newAddress) {
            if (newPassword1 && newPassword2) {
             if(newProfilePicture[0]){
              if (newPassword1 === newPassword2) {
                let userData = new FormData()
                userData.append("first_name", first_name)
                userData.append("last_name", last_name)
                userData.append("password", newPassword1)
                userData.append("email", newEmail)
                userData.append("contact", newContact)
                userData.append("address", newAddress)
                userData.append("photo", newProfilePicture[0])
                userData.append("is_active", true)

                const response = await fetch(`${SERVER_URL}users/${accountType}`, {
                  method: 'POST',
                  body: userData
                })
                if (response.status == 201) {
                  setUsername(newEmail)
                  setPassword(newPassword2)
                  alert(`Your account has been created successfully, please check your email,(${newEmail}) for your login credentials`)
                  navigate('/')
                }
                else {
                  alert('Please try again')
                }
              } else {
                alert("please provide matching passwords");
              }
             }
             else{
              alert('please select an image')
             }
            } else {
              alert("password fields are required");
            }
          } else {
            alert("you are required to provide your address");
          }
        } else {
          alert("please provide your contact");
        }
      } else {
        alert("please provide your email");
      }
    } else {
      alert("please provide the first and last name")
    }
    setLoading(false)
  };


  const Logout = async () => {
    localStorage.removeItem('authtokens')
    setAuthTokens('')
    navigate('/')
    setUsername('')
    alert('You have been signed out')
  }


  const Redirect = (accountType) => {
    setLoading(true)
    switch (accountType) {
      case 'manager':
        navigate("/manager/dashboard")

        break
      case 'dean':
        navigate("/dean/dashboard")
        break
      case 'student':
        navigate("/student/dashboard",)
        break
      default:
        navigate('/')
        break


    }
    setLoading(false)
  }


  let signin = async () => {
      setLoading(true)
      const response = await fetch(`${SERVER_URL}token/`, {
        method: 'POST',
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify({
          email: username,
          password: password
        })
      })
      if (response.status == 200) {
        const data = await response.json()
        localStorage.setItem('authtokens', JSON.stringify(data))
        Redirect(jwtDecode(data.access).groups[0])
      }
      else {
        alert('please provide the correct credentials')
        setUsername('')
        setPassword('')
      }
      setLoading(false)
  };



  return (
    <AuthContext.Provider
      value={{
        username,
        password,
        newAddress,
        newContact,
        newEmail,
        first_name,
        last_name,
        newPassword1,
        newPassword2,
        newProfilePicture,
        AuthTokens,
        user,
        SERVER_URL,
        accountType, 
        //functions
        Redirect,
        setAccountType,
        signup,
        signin,
        setUsername,
        setPassword,
        setNewAddress,
        setLast_name,
        setFirst_name,
        setNewEmail,
        setNewContact,
        setNewPassword1,
        setNewPassword2,
        setNewProfilePicture,
        getUser,
        setUser,
        Logout,
      }}
    >
      {loading ? displayLoader() : children}
    </AuthContext.Provider>
  );
};
export default AuthContext;
