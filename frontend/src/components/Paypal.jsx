import React, { useContext, useEffect, useRef, useState} from 'react';
import DataContext from '../utilities/DataContext';
import AuthContext from '../utilities/AuthContext';
import { jwtDecode } from 'jwt-decode';
import { useNavigate } from 'react-router-dom';
import { FaMoneyBillWave, FaSave } from 'react-icons/fa'


const Paypal = () => {
  let [message, setMessage] = useState('')
  let {currentRoom,displayLoader,getReservations, getRooms, setSearchResults} = useContext(DataContext)
  const [loading, setLoading] = useState(false)
  let navigate = useNavigate()
  let {SERVER_URL} = useContext(AuthContext)
  let [details, setDetails] = useState(false)
  const paypal = useRef();
  const saveBooking =()=>{
    setLoading(true)
    fetch( `${SERVER_URL}getReservations/${currentRoom.hostel}`,{
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body:JSON.stringify({
        hostel: currentRoom.hostel,
        room: parseInt(currentRoom.id),
        amount: 26 * 3750,
        student: jwtDecode((JSON.parse(localStorage.getItem('authtokens')).access)).user_id
      })
    }).then(res=>{
      if(res.status == 201){
        alert('booking made successfully')
        getRooms(currentRoom.hostel)
        getReservations()
        navigate('/student/dashboard')
      }
      else if(res.status == 304){
        alert('you already have a booking')
        setDetails(false)
      }
      else if (res.status == 226){
        alert('Room is full')
        setDetails(false)
      }
      else{
        alert('something went wrong')
        setDetails(false)
      }
    })
    setDetails(false)
    setLoading(false)
  }



  useEffect(() => {
    // Load the PayPal script dynamically
    const script = document.createElement('script');
    script.src = 'https://www.paypal.com/sdk/js?client-id=AbCeQnCkWZD9HySep1ybyvwiEuuMdMKsB8lr6ro1k4Wyt2B64lzu8grud3GIaBxeYMfW_PN1We87WEEq&currency=USD';
    script.async = true;

    const initializePaypalButton = () => {
      window.paypal
        .Buttons({
          createOrder: (data, actions, err) => {
            return actions.order.create({
              intent: 'CAPTURE',
              purchase_units: [
                {
                  room: 'Room',
                  amount: {
                    currency_code: 'USD',
                    value: '26.00',
                  },
                },
              ],
            });
          },
          onApprove: (data, actions) => {
            // Handle the transaction approval
            return actions.order.capture().then((details) => {
          setDetails(true)
            });
          },
          onError: (err) => {
            // Handle errors
            alert('Error during PayPal checkout')
          },
        })
        .render(paypal.current);
    };

    script.onload = () => {
      initializePaypalButton();
    };

    script.onerror = () => {
      setMessage('Check your internet connection')
      
    };

    document.body.appendChild(script);

    return () => {
      
      setTimeout(() => {
        if (script.parentNode) {
          document.body.removeChild(script);
        }
      }, 0);
    };
  }, []); 

  return (
    <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div className="modal-dialog">
       {
        loading ? displayLoader(): <div className="modal-content bg-light">
        <div className="modal-header border-0 bg-light">
          <p className="modal-title text-db fs-5" id="exampleModalLabel">
            {
              !message ?
              <span className="text-Db"></span>
              
              :
            <small className="text-red text-center">{message}</small>
            }
          </p>
          {
            !details ?
          <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>: <button type="button" data-bs-dismiss="modal" aria-label="Close" onClick={saveBooking} className='px-3 py-2 btn bg-Db border-0 btn-primary rounded-edges'><FaMoneyBillWave className='mx-2'/><small>Complete Transaction</small></button>
          }
        </div>
        <div className="modal-body" ref={paypal}></div>
      </div>
       }
      </div>
    </div>
  );
};

export default Paypal;
