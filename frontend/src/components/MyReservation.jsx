import React, { useState, useEffect, useContext } from 'react'
import DataContext from '../utilities/DataContext'
import { FaCheck, FaClock } from 'react-icons/fa'
import moment from 'moment'
const MyReservation = () => {
  function getRoomType(capacity) {
    switch (parseInt(capacity)) {
      case 1:
        return 'single room'
        break
      case 2:
        return 'Double room'
        break
      case 3:
        return 'tripple Room'
        break
      default:
        return 'invalid room type'
    }
  }
  function getRTime(timestamp) {
    const datetime = moment(timestamp);
    const relativeTime = moment(timestamp).fromNow()
    return relativeTime
  }
  let { displayLoader, reservations} = useContext(DataContext)
  let [loading, setLoading] = useState(true)
  useEffect(() => {
    setLoading(false)
  }, [])
  return (
    <div className="container mt-5">
      <h6 className="text-red text-center my-3">Booking <span className="text-Db">(reservation)</span> History</h6>
      <div className=" row container justify-content-center">
        {
          loading ? displayLoader() : reservations.length > 0 ? reservations.map(reservation => <div key={reservation.id} className="card rounded hair-line col-lg-3 hostel p-0 bg-light br-none text-light">
            <div className="card-body px-4 py-3 ">
              
                <h3 className="text-Db">{reservation.hname}</h3>
                <p className="card-title text-secondary">Room <span className="text-danger">{reservation.number}</span></p>
                <p className='text-secondary'>Net price <span className="hair-line rounded px-3 py-2 text-Db">UGX {reservation.price}</span></p>
                <p className='text-secondary'>{getRoomType(reservation.capacity)}</p>
                <p className='text-secondary'>Paid<span className="lead text-Db"> UGX {reservation.amount}</span></p>
                <p className=' d-flex align-items-center text-Db'><FaClock className='text-secondary opacity-25'/><span className="mx-2">{getRTime(reservation.created)}</span></p>
                <p className="bg-red w-50 px-3 py-1 rounded d-flex align-items-center justify-content-center">
                  <FaCheck className='mx-1' />Reserved
                </p>
              
            </div>
          </div>) : <p className='text-center my-5 text-light-blue'>You donot have any reservations</p>
        }
      </div>
    </div>
  )
}

export default MyReservation
