//the useContext function is used to consume data from a context API
import React, { useContext } from "react";
//importing the DataContext file from the specified path
import DataContext from "../utilities/DataContext";
//importing the component SearchRoom from that path
import SearchRoomMngr from "./SearchRoomMngr";
//importing icons FaSignOutAlt and FaBell from that react package
import { FaSignOutAlt, FaBell, FaStar } from "react-icons/fa";
//declaring a functional component to define the UI components
const TopBarMngr = ({hostelname}) => {
  //we are going to use the useContext hook to access the value of the
  //hostel property from the DataContext, here we pass data through that component tree
  let { hostel } = useContext(DataContext);
  return (
    <div
      className="d-flex align-items-center justify-content-between px-1  mx-2 my-2">
    
      <span className="d-flex align-items-center">{hostelname.name} &nbsp;&nbsp;<span className="text-secondary mx-1">{hostelname.rating}</span><FaStar className="text-warning mx-1"/></span>
      <span className="d-flex align-items-center justify-content-between ">
        <SearchRoomMngr />
        <span className="last-div d-flex align-items-center justify-content-between">
          <small>SignOut</small>
          <FaSignOutAlt className="last-div mx-3" />
        </span>
      </span>
    </div>
  );
};

export default TopBarMngr;
