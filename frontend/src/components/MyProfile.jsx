import React, { useContext, useEffect, useState } from "react";
import AuthContext from '../utilities/AuthContext'
import DataContext from "../utilities/DataContext";
import { FaPhoneSlash, FaEnvelope, FaUser } from 'react-icons/fa'
import { jwtDecode } from "jwt-decode";

const MyProfile = () => {
  const { SERVER_URL} = useContext(AuthContext)
  const { displayLoader } = useContext(DataContext)
  const [loading, setLoading] = useState(true)

  let user = jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access)

  useEffect(() => {
    setLoading(true)
    setLoading(false)

  },[loading])
  return (
    <>

      {
        loading ? displayLoader() :<>
            <div className="container">
              <div className="card bg-light">
              <div className="card-body hair-line text-Db  d-flex align-items-center justify-content-center flex-column py-4 rounded">
              {
                user.photo_url ? <div className="rounded-image  rounded-circle m-3">
                  <img
                    src={SERVER_URL + user.photo_url}
                  />
                </div> : <FaUser size={100} className="px-3 rounded-circle bg-db-light py-2" />
              }
              <p>{user.first_name} {user.last_name}</p>

              <p className='text-Db'><FaEnvelope className='mx-1 text-Db d-none d-lg-flex' />{user.email}</p>
              <p className=''><FaPhoneSlash className='mx-1 text-Db card-title' />{user.contact}</p>

              <p></p>
            </div>
              </div>
            </div>
          </>

      }
    </>
  );
};

export default MyProfile;
