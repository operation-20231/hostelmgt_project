import React, { useContext } from "react";
import { jwtDecode } from "jwt-decode";
import {FaAngleDown, FaInstagram, FaLinkedin, FaTwitter } from "react-icons/fa";
import { Link } from "react-router-dom";
import AuthContext from '../utilities/AuthContext'
const Navbar = () => {
  let {SERVER_URL} = useContext(AuthContext)
  return (
    <div className="bg-db">
      <div
        className="py-2 d-flex container text-center align-items-center justify-content-between"
        id="nav"
      >
        <span style={{fontFamily: 'helvetica', fontWeight: 'bold'}}>
          <b className="d-lg-none d-block text-Db">H<span className=" text-red">M</span>S</b>
          <span className="d-lg-block text-center text-Db d-none"><h6>HOSTEL<span className="text-red"> BOOKING</span> SYSTEM</h6></span>
        </span>
        <div className="d-flex align-items-center justify-content-center text-Db">
        <Link className="nav-link text-Db" to={'http://www.instagram.com/v.in.ce.nt._'}><FaInstagram className="mx-2" size={17}/></Link>
        <Link  className="nav-link text-Db" to={'http://www.linkedin.com/kigongovincent81'}><FaLinkedin className="mx-2" size={17}/></Link>
        <Link  className="nav-link text-Db"><FaTwitter className="mx-2" size={17}/></Link>
        <div className="mx-4 d-flex align-items-center">
        <Link className=" rounded-circle small-rounded-image " to={'/profile'}>
          <img src={`${SERVER_URL}${jwtDecode(JSON.parse(localStorage.getItem('authtokens')).access).photo_url}`} alt="" />
        </Link>
        
        <FaAngleDown/>
        </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
