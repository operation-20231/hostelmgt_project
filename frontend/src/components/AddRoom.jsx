import React, { useState ,useContext} from "react";
import Input from "./Input";
import DataContext from "../utilities/DataContext";
import AuthContext from "../utilities/AuthContext";
const AddRoom = () => {
  const [number, setNumber] = useState('');
  const [capacity, setCapacity] = useState('');
  const [price, setPrice] = useState('');
  const [imageUploads, setImageUploads] = useState([]);
  let {displayLoader} = useContext(DataContext)
  let { SERVER_URL } = useContext(AuthContext)
  const [loading, setLoading] = useState(false)

  let {myhostel,getMyHostel}= useContext(DataContext)

  const uploadRoom =async()=> {
    setLoading(true)
    let id = myhostel && myhostel.id
    let uploadData = new FormData();
    for (let i = 0; i < imageUploads.length; i++) {
      uploadData.append(`image${i}`, imageUploads[i]);
    }
    uploadData.append("number", number);
    uploadData.append("capacity", capacity);
    uploadData.append("price", price);
    uploadData.append('hostel', id)
    const response = await fetch(`${SERVER_URL}getRooms/${id}`,{
      method: 'POST',
      body: uploadData,
      headers:{
        'Authorization': `Bearer ${JSON.parse(localStorage.getItem('authtokens')) && JSON.parse(localStorage.getItem('authtokens')).access}`
      }
    })
    setLoading(false)
    if(parseInt(response.status) === 201){
      alert('Room added successfully')
      getMyHostel(setLoading)
    }else{
      alert('failed to upload, please review your input, ensure unique room numbers')
    }
    
    
}


  const [images, setImages] = useState([]);
  const [imagesPreview, setImagesPreview] = useState([]);

  const handleImageChange = (e) => {
    const selectedImages = Array.from(e.target.files);
    setImageUploads(e.currentTarget.files)
    setImages(selectedImages);

    // Display image previews
    const imagePreviews = selectedImages.map((image, index) => (
      <img
        key={index}
        src={URL.createObjectURL(image)}
        alt="Preview"
        style={{ maxWidth: "150px", border: "none", borderRadius: 5 }}
      />
    ));

    setImagesPreview(imagePreviews);
  };

  return (
    <>
    {
      loading ? displayLoader() : <div
      className="modal fade"
      id="addroom"
      tabIndex="-1"
    >
      <div className="modal-dialog">
        <div className="modal-content bg-light">
          <div className="modal-header bg-Db border-0">
            <p className="modal-title">
              Add room
            </p>
            <button
              type="button"
              className="btn-close bg-lighter"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="modal-body">
            <form  className="add-room">
              <Input
              type={'text'}
              field={'Room Number'}
              placeholder={'Room 67'}
              Update={(e)=>setNumber(e.target.value)}
              defaultvalue={number}
              />
              <Input
              type={'number'}
              field={'Capacity'}
              placeholder={'not more than 3'}
              Update={(e)=>setCapacity(e.target.value)}
              defaultvalue={capacity}
              />
              <Input
              type={'number'}
              field={'price'}
              placeholder={'UGX 400000'}
              Update={(e)=>setPrice(e.target.value)}
              defaultvalue={price}
              />
              <div className="mb-3 mx-3">
                <p className="my-2 text-secondary">Select images of the room</p>
                <input
                  type="file"
                  id="input"
                  accept="image/*"
                  className="text-Db py-2 rounded-edges"
                  multiple
                  onChange={handleImageChange}
                />
                <br />
                <br />

                <div className="row" id="imagePreview">
                  {imagesPreview}
                </div>
              </div>
            </form>
          </div>
          <div className=" border-0 my-3 align-items-center justify-content-center d-flex">
            <button
              type="button"
              className="px-2 mx-2 bg-red border-0 py-1 rounded text-light"
              data-bs-dismiss="modal"
            >
              Cancel
            </button>
            <button
              type="button"
              data-bs-dismiss="modal"
              className="px-2 py-1 mx-1 btn btn-primary bg-blue border-0" onClick={uploadRoom}
            >
              Upload
            </button>
          </div>
        </div>
      </div>
    </div>
}</>
  );
};

export default AddRoom;
